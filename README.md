# Demo app for rounding bug in CoordinatorLayout

This app shows how the translation annimation in CoordinatorLayout can casue incorrect states for anyone observing the animated view because of rounding during the process. If the movement between two animation steps is too small **onDependentViewChanged** is not called for behaviors depending on the view.

Behavior of the app with official support lib - values are inorrectly set into random values "close to the final values"

![actual behavior with rect rounding](rect_demo.gif)

Behavior of the app with proposed changes - values are allways correctly set to the final values

![proposed behavior with rectF](rectF_demo.gif)
 
*PR for the proposed change into androidx project can be found here: https://android-review.googlesource.com/c/platform/frameworks/support/+/811813*