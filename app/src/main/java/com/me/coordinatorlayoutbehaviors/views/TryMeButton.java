package com.me.coordinatorlayoutbehaviors.views;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.View;

import com.me.coordinatorlayoutbehaviors.R;

public class TryMeButton extends AppCompatButton implements View.OnClickListener {

    private int mTranslationY;

    public TryMeButton(final Context context) {
        super(context);
        init(context);
    }

    public TryMeButton(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TryMeButton(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(final Context context) {
        if (!isInEditMode()) {
            setOnClickListener(this);
            mTranslationY = context.getResources().getDimensionPixelSize(R.dimen.tryMeButtonTranslation);
        }
    }

    @Override
    public void onClick(final View v) {
        if (getTranslationY() == mTranslationY) {
            animate().translationY(0);
        } else {
            animate().translationY(mTranslationY);
        }
    }
}
